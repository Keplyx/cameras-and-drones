/*
*   This file is part of Cameras and Drones.
*   Copyright (C) 2017  Keplyx
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <sdktools>
#include <sdkhooks>

char droneSound[] = "vehicles/drone_loop_02.wav";
char droneJumpSound[] = "items/nvg_off.wav";
char openDroneSound[] = "weapons/movement3.wav";
char destroyDroneSound[] = "ambient/energy/spark5.wav";

char inDroneModel[] = "models/chicken/festive_egg.mdl"; // must have hitbox or it will use the default player one
char defaultDroneModel[] = "models/props_survival/drone/br_drone.mdl";
char defaultDronePhysModel[] = "models/props/de_inferno/hr_i/ground_stone/ground_stone.mdl";

ArrayList dronesList;
ArrayList dronesModelList;
ArrayList dronesOwnerList;
int activeDrone[MAXPLAYERS + 1][2];
bool isDroneSoundPlaying[MAXPLAYERS + 1];
int fakePlayersListDrones[MAXPLAYERS + 1];

int oldCollisionValueD[MAXPLAYERS + 1];

float droneEyePosOffset = 5.0;
float droneHoverVolume = 0.1;
float droneJumpVolume = 0.2;
float droneDestroyVolume = 0.2;

// Change with cvars
float droneHoverHeight = 5.0;
float droneCheckSize = 5.0;
float droneClosestForward = 10.0;
float droneSpeed = 200.0;
float droneJumpForce = 300.0;
bool useCustomDroneModel = false;
float customDroneModelRot[3];
char customDroneModel[PLATFORM_MAX_PATH];
char customDronePhysModel[PLATFORM_MAX_PATH];


bool isDroneGrounded[MAXPLAYERS + 1];
bool fixedRotation[MAXPLAYERS + 1];
bool isDroneDown[MAXPLAYERS + 1];
bool isDroneMoving[MAXPLAYERS + 1];

bool canDroneJump[MAXPLAYERS + 1];
bool isDroneJumping[MAXPLAYERS + 1];



bool dTacticalShield;

 /**
 * Reset vars to default values for every player.
 */
public void InitDroneVars()
{
	dronesList = new ArrayList();
	dronesModelList = new ArrayList();
	dronesOwnerList = new ArrayList();
	for (int i = 0; i <= MAXPLAYERS; i++)
	{
		for (int j = 0; j < sizeof(activeDrone[]); j++)
		{
			activeDrone[i][j] = -1;
		}
		isDroneSoundPlaying[i] = false;
		fakePlayersListDrones[i] = -1;
		oldCollisionValueD[i] = -1;
		isDroneGrounded[i] = false;
		fixedRotation[i] = false;
		isDroneDown[i] = false;
		isDroneMoving[i] = false;
		canDroneJump[i] = true;
		isDroneJumping[i] = false;
	}
}

 /**
 * Reset vars to default values for the selected player.
 *
 * @param client_index			client to reset.
 */
public void ResetPlayerDrone(int client_index)
{
	if (dronesList != null)
	{
		for (int i = 0; i < dronesList.Length; i++)
		{
			if (dronesOwnerList.Get(i) == client_index)
				DestroyDrone(dronesList.Get(i), true);
		}
	}
	for (int j = 0; j < sizeof(activeDrone[]); j++)
	{
		activeDrone[client_index][j] = -1;
	}
	isDroneSoundPlaying[client_index] = false;
	fakePlayersListDrones[client_index] = -1;
	oldCollisionValueD[client_index] = -1;
	isDroneGrounded[client_index] = false;
	fixedRotation[client_index] = false;
	isDroneDown[client_index] = false;
	isDroneMoving[client_index] = false;
	canDroneJump[client_index] = true;
	isDroneJumping[client_index] = false;
}


 /**
 * Add a new drone to the list.
 *
 * @param drone					drone index.
 * @param model					drone model index.
 * @param client_index			owner client index.
 */
public void AddDrone(int drone, int model, int client_index)
{
	dronesList.Push(drone);
	dronesModelList.Push(model);
	dronesOwnerList.Push(client_index);
}

 /**
 * Removes the given drone from the list.
 *
 * @param drone				drone index.
 */
public void RemoveDroneFromList(int drone)
{
	int i = dronesList.FindValue(drone);
	if (i < 0)
		return;
	dronesList.Erase(i);
	dronesModelList.Erase(i);
	dronesOwnerList.Erase(i);
}

 /**
 * Creates the drone physics model.
 *
 * @param client_index			index of the client.
 * @param pos					position of the drone to create.
 * @param rot					rotation of the drone to create.
 */
public void CreateDrone(int client_index, float pos[3], float rot[3], float vel[3])
{
	// Can be moved, must have a larger hitbox than the drone model (no stuck, easier pickup, easier target)
	int drone = CreateEntityByName("prop_physics_override"); 
	if (IsValidEntity(drone)) {
		SetDronePhysicsModel(drone);
		DispatchKeyValue(drone, "solid", "6");
		SetEntProp(drone, Prop_Data, "m_CollisionGroup", 1); // Stop collisions with players
		//DispatchKeyValue(drone, "overridescript", "mass,100.0,inertia,1.0,damping,1.0,rotdamping ,1.0"); // overwrite params
		DispatchKeyValue(drone, "overridescript", "rotdamping,1000.0"); // Prevent drone rotation
		DispatchSpawn(drone);
		ActivateEntity(drone);
		TeleportEntity(drone, pos, rot, vel);
		
		SDKHook(drone, SDKHook_OnTakeDamage, Hook_TakeDamageGear);
		SetEntityRenderMode(drone, RENDER_NONE);
		CreateDroneModel(client_index, drone);
	}
}

 /**
 * Sets the drone physics model.
 * Uses a custom model if cvar set and custom model valid.
 *
 * @param drone			index of the drone.
 */
public void SetDronePhysicsModel(int drone)
{
	if (useCustomDroneModel && !StrEqual(customDronePhysModel, "", false))
		SetEntityModel(drone, customDronePhysModel);
	else
	{
		SetEntityModel(drone, defaultDronePhysModel);
		SetEntPropFloat(drone, Prop_Send, "m_flModelScale", 2.0);
	}
}

 /**
 * Creates the drone model prop.
 * This prop isn't solid and is parented to the physics model.
 * Uses a custom model if cvar set and custom model valid.
 *
 * @param client_index			index of the client.
 * @param drone					index of the drone.
 */
public void CreateDroneModel(int client_index, int drone)
{
	// This one can be animated/move with player
	int model = CreateEntityByName("prop_dynamic_override"); 
	if (IsValidEntity(model)) {
		if (useCustomDroneModel && !StrEqual(customDroneModel, "", false))
			SetEntityModel(model, customDroneModel);
		else {
			SetEntityModel(model, defaultDroneModel);
			SetEntPropFloat(model, Prop_Send, "m_flModelScale", 0.5);
		}
			
		
		DispatchKeyValue(model, "solid", "0");
		DispatchSpawn(model);
		ActivateEntity(model);
		
		SetVariantString("!activator"); AcceptEntityInput(model, "SetParent", drone, model, 0);
		
		float pos[3], rot[3];
		if (useCustomDroneModel)
			TeleportEntity(model, pos, customDroneModelRot, NULL_VECTOR);
		else
			TeleportEntity(model, pos, rot, NULL_VECTOR);
		
		SDKHook(model, SDKHook_SetTransmit, Hook_SetTransmitGear);
		
		AddDrone(drone, model, client_index);
	}
}

 /**
 * If the drone is grounded, make it move, float, or jump depending on playing input.
 *
 * @param client_index				index of the client.
 * @param pos						hover position for the drone.
 */
public void SetDronePosition(int client_index, float[3] pos)
{
	if (isDroneGrounded[client_index])
	{
		int drone = activeDrone[client_index][0];
		float vel[3], rot[3], nullRot[3];
		//GetEntPropVector(drone, Prop_Send, "m_vecOrigin", pos);
		GetClientEyeAngles(client_index, rot);
		rot[0] = 0.0;
		if (isDroneMoving[client_index] && GetDistanceFrom(drone, pos, rot, -1) > droneClosestForward)
		{
			GetAngleVectors(rot, vel, NULL_VECTOR, NULL_VECTOR);
			ScaleVector(vel, droneSpeed);
		}
		if (isDroneJumping[client_index])
		{
			GetClientEyeAngles(client_index, rot);
			if (rot[0] > -45.0)
				rot[0] = -45.0;
			GetAngleVectors(rot, vel, NULL_VECTOR, NULL_VECTOR);
			ScaleVector(vel, droneJumpForce);
			EmitSoundToAll(droneJumpSound, drone, SNDCHAN_AUTO, SNDLEVEL_CAR, SND_NOFLAGS, droneJumpVolume, SNDPITCH_NORMAL);
			isDroneJumping[client_index] = false;
		}
		TeleportEntity(drone, pos, nullRot, vel);
	}
}

 /**
 * If the player is in a drone, hide the viewmodel and the guns from the hud, and lower the view.
 * Also sets the position of the drone in order for it to fly over the ground.
 *
 * @param client_index			index of the client.
 */
public void Hook_PostThinkDrone(int client_index)
{
	if (!IsClientInDrone(client_index))
		return;
	int drone = activeDrone[client_index][0];
	float groundDistance = DistanceToGround(drone);
	
	LowerDroneView(client_index);
	SetViewModel(client_index, false);
	float rot[3];
	GetClientEyeAngles(client_index, rot);
	rot[2] = 0.0;
	if (useCustomDroneModel)
	{
		for (int i = 0; i < sizeof(rot); i++)
		{
			rot[i] += customDroneModelRot[i];
		}
	}
	TeleportEntity(activeDrone[client_index][1], NULL_VECTOR, rot, NULL_VECTOR); // Model follows player rotation (with custom rotation offset)
	
	isDroneGrounded[client_index] = !(groundDistance > (droneHoverHeight + 1.0));

	if (isDroneDown[client_index] && isDroneSoundPlaying[client_index])
		setDroneSound(client_index, false);
	else if (!isDroneDown[client_index] && !isDroneSoundPlaying[client_index])
		setDroneSound(client_index, true);

	if (isDroneDown[client_index] || !isDroneGrounded[client_index])
		fixedRotation[client_index] = false;
	else
	{
		float pos[3], vel[3];
		GetEntPropVector(drone, Prop_Send, "m_vecOrigin", pos);
		GetEntPropVector(drone, Prop_Data, "m_vecVelocity", vel);
		pos[2] += droneHoverHeight - groundDistance;
		if (!isDroneFixed(drone) && !fixedRotation[client_index])
			FixDroneRotation(client_index, drone);
		
		if (vel[2] >= 0.0)
			SetDronePosition(client_index, pos)
	}
}


 /**
 * Check if the drone rotation is locked at 0,0,0.
 *
 * @param drone			index of the drone.
 * @return				true if drone if locked, false otherwise.
 */
public bool isDroneFixed(int drone)
{
	float rot[3];
	float offset = 1.0;
	GetEntPropVector(drone, Prop_Send, "m_angRotation", rot);
	
	for (int i = 0; i < sizeof(rot); i++)
	{
		if (rot[0] < -offset || rot[0] > offset)
			return false;
	}
	return true;
}

 /**
 * Fixes the player's view to prevent disorienting him when reseting the drone's to 0,0,0.
 * Adds the current player rotation with the drone's to calculate the new player rotation.
 *
 * @param client_index			index of the client.
 * @param drone					index of the drone.
 */
public void FixDroneRotation(int client_index, int drone)
{
	float clientRot[3], droneRot[3], nullRot[3], newRot[3];
	GetEntPropVector(drone, Prop_Send, "m_angRotation", droneRot);
	GetClientEyeAngles(client_index, clientRot);
	for (int i = 0; i < sizeof(droneRot); i++)
	{
		newRot[i] = clientRot[i] + newRot[i];
	}
	newRot[2] = 0.0;
	TeleportEntity(drone, NULL_VECTOR, nullRot, NULL_VECTOR);
	TeleportEntity(client_index, NULL_VECTOR, newRot, NULL_VECTOR);
	fixedRotation[client_index] = true;
}

 /**
 * Lower the player view to match the drone position.
 *
 * @param client_index			index of the client.
 */
public void LowerDroneView(int client_index)
{
	float viewPos[3];
	viewPos[2] = droneEyePosOffset;
	SetEntPropVector(client_index, Prop_Data, "m_vecViewOffset", viewPos);
}

 /**
 * Calculates the distance to the ground from the center of the entity.
 *
 * @param client_index				index of the client.
 * @return 							distance from the entity to the ground. 999.0 if no end point is found.
 */
public float DistanceToGround(int entity_index)
{
	float pos[3], ang[3], rot[3], direction[3], distances[3];
	ang[0] = 90.0; // points to the ground
	ang[1] = 0.0;
	ang[2] = 0.0;
	int client_index = dronesOwnerList.Get(dronesList.FindValue(entity_index));
	float minDistance = 999.0;
	GetEntPropVector(entity_index, Prop_Send, "m_vecOrigin", pos);
	// Get the distance from 3 different points to prevent the drone from getting stuck near a wall.

	GetClientEyeAngles(client_index, rot);
	rot[0] = 0.0;
	GetAngleVectors(rot, direction, NULL_VECTOR, NULL_VECTOR);
	ScaleVector(direction, droneCheckSize);

	distances[0] = GetDistanceFrom(entity_index, pos, ang, 2);
	pos[0] += direction[0];
	pos[1] += direction[1];
	distances[1] = GetDistanceFrom(entity_index, pos, ang, 2);
	pos[0] -= 2*direction[0];
	pos[1] -= 2*direction[1];
	distances[2] = GetDistanceFrom(entity_index, pos, ang, 2);
	for (int i =0; i< sizeof(distances); i++)
	{
		if (distances[i] != -1 && distances[i] < minDistance)
			minDistance = distances[i];
	}
	return minDistance;
}


public float GetDistanceFrom(int entity_index, float origin[3], float rot[3], int coord)
{	
	float distance = 0.0;
	Handle hTrace = TR_TraceRayFilterEx(origin, rot, MASK_ALL, RayType_Infinite, TraceFilterIgnorePlayers, entity_index);
	if(hTrace != INVALID_HANDLE && TR_DidHit(hTrace))
	{
		float endPoint[3];
		TR_GetEndPosition(endPoint, hTrace);
		CloseHandle(hTrace);
		if (coord == -1)
		{
			for (int i = 0; i < sizeof(origin); i++)
			{
				distance += (endPoint[i] - origin[i])*(endPoint[i] - origin[i]);
			}
			distance = SquareRoot(distance);
		}
		else
			distance = FloatAbs(endPoint[coord] - origin[coord])

	}
	else
	{
		PrintToServer("No end point found!");
		distance = -1.0;
	}
	return distance;
}


 /**
 * Filter for trace ray ignoring players and the given data.
 */
public bool TraceFilterIgnorePlayers(int entity_index, int mask, any data)
{
	if((entity_index >= 1 && entity_index <= MaxClients) || entity_index == data)
	{
		return false;
	}
	return true;
} 

 /**
 * Teleports the player to the selected drone.
 * It creates a fake player at his old position.
 * The teleported player is frozen, not solid, and invicible.
 *
 * @param client_index			index of the client.
 * @param drone					index of the drone.
 */
public void TpToDrone(int client_index, int drone)
{
	if (dTacticalShield)
		SetHidePlayerShield(client_index, true);
	
	// Allow for drone to drone switch
	if (!IsClientInDrone(client_index))
	{
		CreateFakePlayer(client_index, false);
		EmitSoundToClient(client_index, openDroneSound, SOUND_FROM_PLAYER, SNDCHAN_AUTO, SNDLEVEL_NORMAL, SND_NOFLAGS, SNDVOL_NORMAL, SNDPITCH_NORMAL);
	}
	else
		StopSound(activeDrone[client_index][0], SNDCHAN_AUTO, droneSound)
	// Set active
	activeDrone[client_index][0] = drone;
	activeDrone[client_index][1] = dronesModelList.Get(dronesList.FindValue(drone));
	// Set appearance
	SetEntityModel(client_index, inDroneModel); // Set to a small model to prevent collisions/shots
	SetGearScreen(client_index, true);
	SetEntityMoveType(client_index, MOVETYPE_NOCLIP);
	
	// Set pos
	SetVariantString("!activator"); AcceptEntityInput(client_index, "SetParent", drone, client_index, 0);
	float pos[3], rot[3];
	GetEntPropVector(activeDrone[client_index][1], Prop_Send, "m_angRotation", rot);
	rot[2] = 0.0;
	if (useCustomDroneModel)
	{
		for (int i = 0; i < sizeof(rot); i++)
		{
			rot[i] -= customDroneModelRot[i];
		}
	}
	TeleportEntity(client_index, pos, rot, NULL_VECTOR); // Get old rotation back (with custom rotation offset)
	// Set collisions
	oldCollisionValueD[client_index] = GetEntData(client_index, GetCollOffset(), 1);
	SetEntData(client_index, GetCollOffset(), 2, 4, true);
	SetEntProp(client_index, Prop_Send, "m_nHitboxSet", 2);
	
	// Hooks
	SDKHook(client_index, SDKHook_SetTransmit, Hook_SetTransmitPlayer);
	SDKHook(client_index, SDKHook_PostThink, Hook_PostThinkDrone);
	SDKHook(client_index, SDKHook_OnTakeDamage, Hook_TakeDamagePlayer);
	
	// Sound
	setDroneSound(client_index, true);
	
	HideHudGuns(client_index);
}

 /**
 * Teleports the player from the drone to his old postion (fake player position).
 * It deletes the fake player.
 * Player gets normal properties (collisions, movement).
 * If player is not in a drone, do not delete fake player and do not teleport player.
 *
 * @param client_index			index of the client.
 */
public void ExitDrone(int client_index)
{
	SetGearScreen(client_index, false);
	SetViewModel(client_index, true);
	SetEntityMoveType(client_index, MOVETYPE_WALK);
	
	SDKUnhook(client_index, SDKHook_SetTransmit, Hook_SetTransmitPlayer);
	SDKUnhook(client_index, SDKHook_PostThink, Hook_PostThinkDrone);
	SDKUnhook(client_index, SDKHook_OnTakeDamage, Hook_TakeDamagePlayer);
	
	// Set collisions
	SetEntData(client_index, GetCollOffset(), oldCollisionValueD[client_index], 1, true);
	SetEntProp(client_index, Prop_Send, "m_nHitboxSet", 0);
	// Sound
	setDroneSound(client_index, false);
	
	if (IsClientInDrone(client_index) && IsValidEdict(fakePlayersListDrones[client_index]))
	{
		// Set appearance
		char modelName[PLATFORM_MAX_PATH];
		GetEntPropString(fakePlayersListDrones[client_index], Prop_Data, "m_ModelName", modelName, sizeof(modelName));
		SetEntityModel(client_index, modelName);
		// Set pos
		AcceptEntityInput(client_index, "SetParent");
		float pos[3], rot[3];
		GetEntPropVector(fakePlayersListDrones[client_index], Prop_Send, "m_vecOrigin", pos);
		GetEntPropVector(fakePlayersListDrones[client_index], Prop_Send, "m_angRotation", rot);
		TeleportEntity(client_index, pos, rot, NULL_VECTOR);
		RemoveEdict(fakePlayersListDrones[client_index]);
		EmitSoundToClient(client_index, openDroneSound, SOUND_FROM_PLAYER, SNDCHAN_AUTO, SNDLEVEL_NORMAL, SND_NOFLAGS, SNDVOL_NORMAL, SNDPITCH_NORMAL);
	}
	
	fakePlayersListDrones[client_index] = -1;
	activeDrone[client_index][0] = -1;
	activeDrone[client_index][1] = -1;
	
	if (dTacticalShield)
		SetHidePlayerShield(client_index, false);
	
	ShowHudGuns(client_index);
}

 /**
 * Destroys the selected drone.
 * If a player is using it, closes the drone first.
 *
 * @param drone					index of the drone.
 * @param isSilent				whether to play a destroy sound.
 */
public void DestroyDrone(int drone, bool isSilent)
{
	if (!isSilent)
	{
		float pos[3];
		GetEntPropVector(drone, Prop_Send, "m_vecOrigin", pos);
		EmitSoundToAll(destroyDroneSound, SOUND_FROM_PLAYER, SNDCHAN_AUTO, SNDLEVEL_NORMAL, SND_NOFLAGS,  droneDestroyVolume, SNDPITCH_NORMAL, -1, pos);
	}
	for (int i = 1; i <= MAXPLAYERS; i++)
	{
		if (activeDrone[i][0] == drone && IsValidClient(i))
			CloseDrone(i);
	}
	
	if (IsValidEdict(drone))
		RemoveEdict(drone);
	if (IsValidEdict(dronesModelList.Get(dronesList.FindValue(drone))))
		RemoveEdict(dronesModelList.Get(dronesList.FindValue(drone)));
	
	RemoveDroneFromList(drone);
}

 /**
 * Checks whether the given player is using his drone or not.
 *
 * @param client_index		index of the client.
 * @return					true if the player is using his drone, false otherwise.
 */
public bool IsClientInDrone(int client_index)
{
	return activeDrone[client_index][0] > MAXPLAYERS;
}


public void setDroneSound(int client_index, bool enabled)
{
	if (enabled)
	{
		EmitSoundToAll(droneSound, activeDrone[client_index][0], SNDCHAN_AUTO, SNDLEVEL_NORMAL, SND_NOFLAGS, droneHoverVolume, SNDPITCH_NORMAL);
		isDroneSoundPlaying[client_index] = true;
	}
	else
	{
		StopSound(activeDrone[client_index][0], SNDCHAN_AUTO, droneSound);
		isDroneSoundPlaying[client_index] = false;
	}
}