# Changelog

* **Version 1.2.3**
  * Improved drone controls
  * Imporved drone sounds
  * Fixed being able to deploy gear while using one

* **Version 1.2.2**
  * Changed default drone model

* **Version 1.2.1**
  * Fixed plugin for panorama
  * Fixed hud guns not hiding
  * Use keyvalues files for custom models

* **Version 1.2.0**
  * Added cd_deploy command to deploy your gear
  * Changed cd_cam to cd_toggle
  * Changed some cvar names
  * Stopped using tactical grenade as gear. Can deploy gear while holding any weapon and using cd_deploy
  * Added buy time
  * Can keep gear between rounds
  * Improved hint texts

* **Version 1.1.7**
  * Improved compatibility with [Tactical Shield](https://keplyx.github.io/TacticalShield/index.html)

* **Version 1.1.6**
  * Fixed errors when exiting/entering gear

* **Version 1.1.5**
  * Added overlay while in gear
  * Improved drone rotation control
  * Documented code

* **Version 1.1.4**
  * Fixed player not taking damage after finishing round in gear
  * Fixed plugin crash on start when custom_models file was not present

* **Version 1.1.3**
  * Changed native description

* **Version 1.1.2**
  * Added ability to choose whether to use tagrenades when no gear bought
  * Changed buy restriction: you can now buy as much gear as you can place

* **Version 1.1.0**
  * Added support for custom camera and drone models
  * Added new commands in the help
  * Added cvar to change the drone hover height


* **Version 1.0.2**
  * Added gear override commands + natives
  * Added cvar to choose whether to use cameras angles for the player
  * Fixed menu closing error when finishing a round inside camera/drone

* **Version 1.0.0**
  * Initial release
 
