# Cameras and Drones
This plugin adds cameras and drones to the game. Cameras are designed to be used as defensive gear, whereas drones should be used while attacking.

Use this gear to lay traps to your enemies! 

It is similar in some ways to Rainbow six siege.

## See the [wiki](https://gitlab.com/Keplyx/cameras-and-drones/wikis/home) for more information


## Installation

Simply download **[cameras-and-drones.smx](https://gitlab.com/Keplyx/cameras-and-drones/raw/master/plugins/cameras-and-drones.smx)** and place it in your server inside "csgo/addons/sourcemod/plugins/".
More information [here](https://gitlab.com/Keplyx/cameras-and-drones/wikis/installation).

## [Cvars](https://gitlab.com/Keplyx/cameras-and-drones/blob/master/cameras-and-drones.cfg)

## [Commands](https://gitlab.com/Keplyx/cameras-and-drones/wikis/commands)

## [Changelog](https://gitlab.com/Keplyx/cameras-and-drones/blob/master/CHANGELOG.md)

### Creator: Keplyx
